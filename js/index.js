// task 1
const allParagraphs = document.querySelectorAll('p');

allParagraphs.forEach(paragraph => paragraph.style.background = "#ff0000");

// task 2
const optionsList = document.getElementById("optionsList");
const parentNodeForOptionsList = optionsList.parentNode;
const childrenOfOptionsList = optionsList.childNodes;

console.log(optionsList);
console.log(parentNodeForOptionsList);
console.log(childrenOfOptionsList);

findNameAndTypeOfcolection(childrenOfOptionsList);

function findNameAndTypeOfcolection(colection) {
  for (let el of colection) {
    console.log( `Name: ${el.nodeName}, type: ${el.nodeType}`);
  }
}

// task 3
const testParagraph = document.querySelector("#testParagraph");
testParagraph.innerHTML = "This is a paragraph";

//task 4-5
const headerEl = document.querySelector(".main-header");
console.log(headerEl.children);

addNewClassForColection(headerEl, "nav-item");

function addNewClassForColection(colection, newClass) {
  for (let el of colection.children) {
    el.classList.add(newClass);
  }
}

//task 6
const titles = document.querySelectorAll(".section-title");

removeClassOfColection(titles, "section-title");

function removeClassOfColection(colection, deletedClass) {
    for (let item of colection) {
     item.classList.remove(deletedClass)
  }
}
